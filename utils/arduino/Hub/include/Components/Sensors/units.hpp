#ifndef COMPONENTS_SENSORS_UNITS
#define COMPONENTS_SENSORS_UNITS

#include <Arduino.h>

typedef int32_t Millicelcius;

typedef int32_t Pascal;

typedef int32_t MicroradianPerSecond;

typedef int32_t MicrometerPerSecondSquared;

typedef int32_t Nanotesla;

#endif
