#include <Controllers/Empty.hpp>

void Controllers::Empty::execute(Emitter* hub, int32_t* data, int32_t length){
  (void) hub;
  (void) data;
  (void) length;
  // do nothing
}
